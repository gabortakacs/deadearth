﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameMaster : Singleton<GameMaster> {

	[SerializeField] private float _normalTimeScale = 1.0f;
	[SerializeField] private float _pausedTimeScale = 0.000001f;

	private bool _isPaused = false;
	private bool _isGameOver = false;

	private MonoBehaviour[] _playerScripts;

	public bool IsPaused { get { return _isPaused; } }
	public bool IsGameOver { get { return _isGameOver; } }

	private void Awake()
	{
		Time.timeScale = _normalTimeScale;
		int highestLevel = PlayerPrefs.GetInt("CurrentLevel", 0);
		if(highestLevel < SceneManager.GetActiveScene().buildIndex)
		{
			PlayerPrefs.SetInt("CurrentLevel", SceneManager.GetActiveScene().buildIndex);
		}
	}

	private void Start()
	{
		Player.Instance.OnPlayerDied += OnPlayerDied;
		_playerScripts = Player.Instance.GetComponentsInChildren<MonoBehaviour>();
	}

	public void GameOver()
	{
		Player.Instance.PlayerContoller.MouseLook.SetCursorLock(false);
		_isGameOver = true;
		MenuMaster.Instance.GameOverMenu.SetActive(true);
		Time.timeScale = _pausedTimeScale;
		SetEnabledPlayerScripts(false);
	}

	public void OnPlayerDied()
	{
		GameOver();
	}

	public void LoadScene(int index)
	{
		SceneManager.LoadScene(index);
	}

	//public void LoadScene(string name)
	//{
	//	SceneManager.LoadScene(name);
	//}

	public void RestartScene()
	{
		LoadScene(SceneManager.GetActiveScene().buildIndex);
	}

	public void QuitGame()
	{
		Debug.Log("Quit Game");
#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
#else
		Application.Quit();
#endif
	}

	public void Pause()
	{
		Player.Instance.PlayerContoller.MouseLook.SetCursorLock(false);
		_isPaused = true;
		Time.timeScale = _pausedTimeScale;
		SetEnabledPlayerScripts(false);
	}

	public void Resume()
	{
		SetEnabledPlayerScripts(true);
		Time.timeScale = _normalTimeScale;
		_isPaused = false;
		Player.Instance.PlayerContoller.MouseLook.SetCursorLock(true);
	}

	private void SetEnabledPlayerScripts(bool enabled)
	{
		foreach (MonoBehaviour script in _playerScripts)
		{
			if(typeof(Weapon) != script.GetType())
				script.enabled = enabled;
		}
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
		if(!Player.IsNull)
			Player.Instance.OnPlayerDied -= OnPlayerDied;
	}
}
