﻿using System.Collections.Generic;
using UnityEngine;

public class GameResourceManager : Singleton<GameResourceManager>
{
	[SerializeField] private int _maxDeadZombieNumber = 10;

	private Dictionary<int, GoapNPC> _goapNPCs = new Dictionary<int, GoapNPC>();
	private Queue<GameObject> _deadZombies = new Queue<GameObject>();

	public void RegisterGoapNPC(int key, GoapNPC goapNPC)
	{
		if(!_goapNPCs.ContainsKey(key))
		{
			_goapNPCs[key] = goapNPC;
		}
	}

	public GoapNPC GetGoapNPC(int key)
	{
		GoapNPC goapNPC = null;
		if(_goapNPCs.TryGetValue(key, out goapNPC))
		{
			return goapNPC;
		}
		return null;
	}

	public void AddDeadZombie(GameObject gameObject)
	{
		_deadZombies.Enqueue(gameObject);
		if(_deadZombies.Count > _maxDeadZombieNumber)
		{
			GameObject zombie = _deadZombies.Dequeue();
			Destroy(zombie);
		}
	}

}
