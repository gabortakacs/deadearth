﻿using UnityEngine;
using UnityEngine.UI;

public class MenuMaster : Singleton<MenuMaster>
{
	[SerializeField] private GameObject _startMenu;
	[SerializeField] private GameObject _pauseMenu;
	[SerializeField] private GameObject _gameOverMenu;
	[SerializeField] private GameObject _optionsMenu;
	[SerializeField] private Toggle _soundToogle;
	[SerializeField] private GameObject _playerHUD;
	[SerializeField] private bool _startMenuOnStart = false;

	public GameObject GameOverMenu { get { return _gameOverMenu; } }

	public void Start()
	{
		if (_startMenuOnStart)
		{
			_startMenu.SetActive(true);
		}
		if(_soundToogle)
		{
			_soundToogle.isOn = SoundMaster.Instance.SoundEnabled;
		}
	}

	public void Update()
	{
		if (Input.GetButtonDown("Cancel") && !GameMaster.Instance.IsGameOver && !_startMenuOnStart)
		{
			if (!GameMaster.Instance.IsPaused)
				Pause();
			else
				Resume();
		}
	}

	public void StartGame()
	{
		int nextLevel = PlayerPrefs.GetInt("CurrentLevel", 1);
		GameMaster.Instance.LoadScene(nextLevel > 0 ? nextLevel : 1);
	}

	public void Resume()
	{
		_pauseMenu.SetActive(false);
		_optionsMenu.SetActive(false);
		//if (_playerHUD)
		//	_playerHUD.SetActive(true);
		GameMaster.Instance.Resume();
	}
	
	public void QuitGame()
	{
		GameMaster.Instance.QuitGame();
	}

	public void Pause()
	{
		_pauseMenu.SetActive(true);
		//if (_playerHUD)
		//	_playerHUD.SetActive(false);
		GameMaster.Instance.Pause();
	}

	public void Restart()
	{
		GameMaster.Instance.RestartScene();
	}

	public void Options()
	{
		_optionsMenu.SetActive(true);
		if (_gameOverMenu)
			_gameOverMenu.SetActive(false);
		if (_playerHUD)
			_pauseMenu.SetActive(false);
		if (_startMenu)
			_startMenu.SetActive(false);
	}

	public void BackFromOptions()
	{
		_optionsMenu.SetActive(false);
		if (_startMenuOnStart)
		{
			_startMenu.SetActive(true);
		}
		else if (GameMaster.Instance.IsPaused)
		{
			_pauseMenu.SetActive(true);
		}
		else
		{
			_gameOverMenu.SetActive(true);
		}
	}

	public void SoundToogleSwitched(bool toState)
	{
		SoundMaster.Instance.SoundEnabled = toState;
	}
}
