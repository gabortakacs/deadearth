﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHUDController : MonoBehaviour 
{
	[SerializeField] private Slider _hpSlider;
	[SerializeField] private Gradient _hpColor;
	[SerializeField] private Text _pickUpText;
	[SerializeField] private Text _currentBulletInMagazineText;
	[SerializeField] private Text _maxBulletInMagazineText;
	[SerializeField] private Text _allBulletText;
	[SerializeField] private GameObject _reloadBar;
	[SerializeField] private Image _reloadingImage;
	[SerializeField] private Color _interruptColor = Color.red;
	[SerializeField] private float _reloadBarOnDurationAfterInterrupt = 0.5f;

	Color _reloadingImageDefaultColor;
	private Image _hpSliderImage;
	private Coroutine _currentReloadCoroutin = null;
	private Coroutine _currentInterruptCoroutin = null;

	private void Awake()
	{
		Player.Instance.OnPlayerHealthChanged += SetPlayerHealthBar;
		PickUpObject.OnPlayerInRange += ShowPickUpText;
		Weapon.OnBulletNumberInMagazineChanged += OnBulletInMagazineChanged;
		Weapon.OnInterruptReloading += OnInterruptReloading;
		Weapon.OnStartReloading += OnStartReloading;
		WeaponController.OnWeaponChanged += OnWeaponChanged;
		_reloadingImageDefaultColor = _reloadingImage.color;
	}

	private void Start()
	{
		_hpSliderImage = _hpSlider.fillRect.GetComponent<Image>();
		SetPlayerHealthBar();
	}

	private void Update()
	{
	}

	private void SetPlayerHealthBar()
	{
		float playerHealthProportion = Player.Instance.Health / Player.Instance.MaxHealth;
		_hpSlider.value = playerHealthProportion;
		_hpSliderImage.color = _hpColor.Evaluate(playerHealthProportion);
	}

	private void ShowPickUpText(bool show)
	{
		_pickUpText.gameObject.SetActive(show);
	}

	void OnBulletInMagazineChanged(int current, int maxInMagazine, int all)
	{
		_currentBulletInMagazineText.text = current.ToString();
		_maxBulletInMagazineText.text = "/ " + maxInMagazine.ToString();
		_allBulletText.text = all.ToString();
	}

	void OnStartReloading(float duration)
	{
		_currentReloadCoroutin = StartCoroutine(Reloading(duration));
		if (_currentInterruptCoroutin != null)
			StopCoroutine(_currentInterruptCoroutin);
	}

	IEnumerator Reloading(float duration)
	{
		_reloadingImage.color = _reloadingImageDefaultColor;
		_reloadBar.SetActive(true);
		_reloadingImage.fillAmount = 0.0f;
		float timer = 0;
		while(timer < duration)
		{
			timer += Time.deltaTime;
			_reloadingImage.fillAmount = timer / duration;
			yield return null;
		}
		_reloadBar.SetActive(false);
	}

	void OnInterruptReloading()
	{
		_currentInterruptCoroutin = StartCoroutine(Interrupting(_reloadBarOnDurationAfterInterrupt));
		if (_currentReloadCoroutin != null)
			StopCoroutine(_currentReloadCoroutin);
	}

	IEnumerator Interrupting(float duration)
	{
		_reloadingImage.color = _interruptColor;
		yield return new WaitForSeconds(duration);
		_reloadBar.SetActive(false);
	}


	void OnWeaponChanged()
	{
		_reloadBar.SetActive(false);
	}

	private void OnDestroy()
	{
		if(!Player.IsNull)
			Player.Instance.OnPlayerHealthChanged -= SetPlayerHealthBar;
		PickUpObject.OnPlayerInRange -= ShowPickUpText;
		Weapon.OnBulletNumberInMagazineChanged -= OnBulletInMagazineChanged;
		Weapon.OnInterruptReloading -= OnInterruptReloading;
		Weapon.OnStartReloading -= OnStartReloading;
		WeaponController.OnWeaponChanged -= OnWeaponChanged;
	}
}
