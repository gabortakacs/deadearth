﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundMaster : Singleton<SoundMaster> {

	[SerializeField] private AudioSource _rainBackgroundSound;

	private bool _soundEnabled;

	private Queue<AudioSource> _loopSounds = new Queue<AudioSource>();

	public bool SoundEnabled
	{
		get { return _soundEnabled; }
		set
		{
			_soundEnabled = value;
			PlayerPrefs.SetInt("SoundEnabled", value ? 1 : 0);
			if (!value)
				StopAllLoopedSounds();

			Debug.Log("S: " + _soundEnabled);
		}
	}

	void Awake () {
		_soundEnabled = PlayerPrefs.GetInt("SoundEnabled", 1) == 0 ? false : true;
		if(_rainBackgroundSound)
			PlaySound(_rainBackgroundSound);
	}

	public void PlaySound(AudioSource audio)
	{
		if (_soundEnabled)
		{
			audio.Play();
			if(audio.loop)
			{
				_loopSounds.Enqueue(audio);
			}
		}
	}

	public void PlayOneShotSound(AudioSource audio)
	{
		if (_soundEnabled)
		{
			audio.PlayOneShot(audio.clip);
			if (audio.loop)
			{
				_loopSounds.Enqueue(audio);
			}
		}
	}
	private void StopAllLoopedSounds()
	{
		while(_loopSounds.Count != 0)
		{
			AudioSource audio = _loopSounds.Dequeue();
			audio.Stop();
		}
	}

}
