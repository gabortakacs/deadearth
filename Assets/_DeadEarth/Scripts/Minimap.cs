﻿using System;
using UnityEngine;

public class Minimap : MonoBehaviour {


	[SerializeField] private float _height;
	[SerializeField] private Transform _target;
	[SerializeField] private Camera _minimapCamera;

	[SerializeField] private GameObject _replacementTree;
	[SerializeField] private Terrain _terrain;

	private Terrain _minimapTerrain;

	private void Awake()
	{
		InitMinimapTerrain();
		ReplaceTerrainTrees();
		_minimapCamera.orthographicSize = _height;
	}

	void LateUpdate ()
	{
		UpdatePosition();
	}

	private void InitMinimapTerrain()
	{
		_minimapTerrain = Instantiate(_terrain);
		_minimapTerrain.gameObject.transform.localScale = _terrain.gameObject.transform.localScale;
		_minimapTerrain.gameObject.transform.rotation = _terrain.gameObject.transform.rotation;
		_minimapTerrain.gameObject.transform.position = _terrain.gameObject.transform.position;
		_minimapTerrain.gameObject.layer = LayerMasks.MinimapSingleLayerMask;
		_minimapTerrain.drawTreesAndFoliage = false;
	}

	private void ReplaceTerrainTrees()
	{
		for(int i = 0; i < _minimapTerrain.terrainData.treeInstances.Length; ++i)
		{
			GameObject replacementTree = Instantiate(_replacementTree);
			replacementTree.transform.position = Vector3.Scale(_minimapTerrain.terrainData.treeInstances[i].position, _terrain.terrainData.size);
			replacementTree.layer = LayerMasks.MinimapSingleLayerMask;
			replacementTree.transform.parent = _minimapTerrain.transform;
		}
	}

	void UpdatePosition()
	{
		Vector3 pos = _target.position;
		pos.y += _height;
		transform.position = pos;
		transform.rotation = Quaternion.Euler(90f, _target.eulerAngles.y, 0f);
	}
}
