﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public delegate void AITargetChanged();

[RequireComponent(typeof(NavMeshAgent), typeof(Animator), typeof(GoapAgent))]
public abstract class GoapNPC : NPC, IGoap
{
	[Header("Goap NPC")]
	[SerializeField] [Range(10.0f, 360.0f)] protected float _fov = 50.0f;
	[SerializeField] [Range(0.0f, 1.0f)] protected float _hearing = 1.0f;
	[SerializeField] [Range(0.0f, 1.0f)] protected float _sight = 0.5f;
	[SerializeField] protected SphereCollider _sensorTrigger;

	protected int _speedHash = Animator.StringToHash("Speed");
	protected int _attackHash = Animator.StringToHash("Attack");

	protected float _speed;
	protected int _rootPositionRefCount = 0;
	public int _rootRotationRefCount = 0;
	protected int _attackType = 0;
	protected AITarget _visualThreat = new AITarget();
	protected AITarget _audioThreat = new AITarget();
	protected AITarget _target = new AITarget();

	protected Animator _animator;
	protected GoapAgent _goapAgent;
	protected NavMeshAgent _navAgent;
	protected Collider _collider;

	protected List<Rigidbody> _bodyParts = new List<Rigidbody>();

	public int AttackType { get { return _attackType; } set { _attackType = value; } }
	public float Fov { get { return _fov; } }
	public float Sight { get { return _sight; } }
	public float Speed { get { return _speed; } set { _speed = value; } }
	public bool UseRootPosition { get { return _rootPositionRefCount > 0; } }
	public bool UseRootRotation { get { return _rootRotationRefCount > 0; } }
	//public AITarget VisualThreat { get { return _visualThreat; } set { _visualThreat = value; } }
	//public AITarget AudioThreat { get { return _audioThreat; } set { _audioThreat = value; } }
	public GoapAgent GoapAgent { get { return _goapAgent; } }
	public Animator Animator { get { return _animator; } }
	public AITarget Target { get { return _target; } }
	public NavMeshAgent NavAgent { get { return _navAgent; } }
	public Vector3 SensorPosition
	{
		get
		{
			Vector3 point = _sensorTrigger.transform.position;
			point.x += _sensorTrigger.center.x * _sensorTrigger.transform.lossyScale.x;
			point.y += _sensorTrigger.center.y * _sensorTrigger.transform.lossyScale.y;
			point.z += _sensorTrigger.center.z * _sensorTrigger.transform.lossyScale.z;
			return point;
		}
	}
	public float SensorRadius
	{
		get
		{
			float maxScale = Mathf.Max(_sensorTrigger.transform.lossyScale.x, _sensorTrigger.transform.lossyScale.y);
			return _sensorTrigger.radius * Mathf.Max(maxScale, _sensorTrigger.transform.lossyScale.z);
		}
	}

	public abstract bool MoveAgent(GoapAction nextAction);
	public abstract void ActionsFinished();
	public abstract void PlanAborted(GoapAction aborter);
	public abstract void PlanFailed(HashSet<KeyValuePair<string, object>> failedGoal);
	public abstract void PlanFound(HashSet<KeyValuePair<string, object>> goal, Queue<GoapAction> actions);
	public abstract HashSet<KeyValuePair<string, object>> GetWorldState();
	public abstract HashSet<KeyValuePair<string, object>> CreateGoalState();
	public abstract void OnTriggerEvent(AITriggerEventType eventType, Collider other);

	public event AITargetChanged OnAITargetChanged;

	protected virtual void Awake()
	{
		_sensorTrigger.GetComponent<AISensor>().GoapNPC = this;
		_navAgent = this.GetComponent<NavMeshAgent>();
		_animator = this.GetComponent<Animator>();
		_goapAgent = this.GetComponent<GoapAgent>();
		_collider = this.GetComponent<Collider>();

		Rigidbody[] bodies = GetComponentsInChildren<Rigidbody>();

		foreach (Rigidbody bodyPart in bodies)
		{
			if (bodyPart != null && bodyPart.gameObject.layer == LayerMasks.BodyPartSingleLayerMask)
			{
				_bodyParts.Add(bodyPart);
				GameResourceManager.Instance.RegisterGoapNPC(bodyPart.GetInstanceID(), this);
			}
		}
	}

	protected virtual void Start()
	{
		SetToAllSMLink();
		GameResourceManager.Instance.RegisterGoapNPC(_collider.GetInstanceID(), this);
		_visualThreat.Clear();
		_audioThreat.Clear();
		ClearTarget();
	}

	//TODO only for debug
	public string visualName;
	public string audioName;
	public string targetName;

	protected virtual void Update()
	{
		visualName = _visualThreat.Type.ToString();
		audioName = _audioThreat.Type.ToString();
		SetNextTarget();
		targetName = _target.Type.ToString();
		_visualThreat.Clear();
		_audioThreat.Clear();
		UpdateAnimatorParams();
	}

	public virtual void OnAnimatorMove()
	{
		GoapAction action = _goapAgent.CurrentAction;
		if (action == null)
			return;
		action.OnAnimatorUpdated();
	}

	public virtual void OnAnimatorIK(int layerIndex)
	{
		GoapAction action = _goapAgent.CurrentAction;
		if (action == null)
			return;
		action.OnAnimatorIKUpdated();
	}

	public void ClearTarget()
	{
		_target.Clear();
	}

	public void AddRootMotionRequest(int rootPosition, int rootRotation)
	{
		_rootPositionRefCount += rootPosition;
		_rootRotationRefCount += rootRotation;
	}

	public virtual void UpdateAnimatorParams()
	{
		_animator.SetFloat(_speedHash, _speed);
		_animator.SetInteger(_attackHash, _attackType);
	}

	public virtual void NavAgentControl(GoapAction action)
	{
		_navAgent.updatePosition = action.UpdateNavAgentPosition;
		_navAgent.updateRotation = action.UpdateNavAgentRotation;
	}

	protected void NavAgentControl(bool positionUpdate, bool rotationUpdate)
	{
		_navAgent.updatePosition = positionUpdate;
		_navAgent.updateRotation = rotationUpdate;
	}

	protected virtual bool ColliderIsVisible(Collider other, out RaycastHit hitInfo, int layerMask, float sightModifier)
	{
		hitInfo = new RaycastHit();

		Vector3 head = SensorPosition;
		Vector3 direction = other.transform.position - head;
		float angle = Vector3.Angle(direction, transform.forward);

		if (angle > _fov * 0.5f)
			return false;

		RaycastHit[] hits = Physics.RaycastAll(head, direction.normalized, SensorRadius * sightModifier, layerMask);

		float closestColliderDistance = float.MaxValue;
		Collider closestCollider = null;
		foreach (RaycastHit hit in hits)
		{
			if (hit.distance < closestColliderDistance)
			{
				if (hit.transform.gameObject.layer == LayerMasks.BodyPartSingleLayerMask)
				{
					GoapNPC target = GameResourceManager.Instance.GetGoapNPC(hit.rigidbody.GetInstanceID());
					//if (_stateMachine != GameSceneManager.instance.GetAIStateMachine(hit.rigidbody.GetInstanceID()))
					if (target == null || target.GetInstanceID() != GetInstanceID())
					{
						closestColliderDistance = hit.distance;
						closestCollider = hit.collider;
						hitInfo = hit;
					}
				}
				else
				{
					closestColliderDistance = hit.distance;
					closestCollider = hit.collider;
					hitInfo = hit;
				}
			}
		}
		return closestCollider && closestCollider.gameObject == other.gameObject;
	}

	protected virtual void SetNextTarget()
	{
		AITarget possibleTarget = AITarget.GetHigherPrio(_visualThreat, _audioThreat);
		if (possibleTarget.Collider == null)
			return;
		int result = AITarget.Compare(_target, possibleTarget);
		if (result == -1)
		{
			SetNewTarget(possibleTarget);
		}
		else if(result == 0 && _target.Type != AITargetType.Visual_Player &&  _target.Collider.GetInstanceID() != possibleTarget.Collider.GetInstanceID())
		{
			_target.Distance = _target.Collider ? Vector3.Distance(transform.position, _target.Collider.transform.position) : _target.Type == AITargetType.Audio ? _target.Distance : Mathf.Infinity;
			if(_target.Distance > possibleTarget.Distance)
			{
				SetNewTarget(possibleTarget);
			}
		}
	}

	private void SetNewTarget(AITarget newTarget)
	{
		//TODO Interrupt act plan
		_target = newTarget;
		if (OnAITargetChanged != null)
			OnAITargetChanged.Invoke();
	}

	private void SetToAllSMLink()
	{
		AIStateMachineLink[] scripts = _animator.GetBehaviours<AIStateMachineLink>();
		foreach (AIStateMachineLink script in scripts)
		{
			script.GoapNPC = this;
		}
	}

}
