﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GoapZombie : GoapEnemy
{
	[Header("Zombie")]
	[SerializeField] [Range(0.0f, 1.0f)] private float _satisfaction = 0.8f;
	[SerializeField] [Range(0.0f, 1.0f)] private float _satisfactionThreshold = .2f;
	[SerializeField] [Range(0.0f, 0.01f)] private float _satisfactionDecrease = .01f;
	[SerializeField] [ReadOnly] private float _slerpSpeed = 4.0f;
	//[SerializeField] [Range(0, 180)] private int _waypointAngleThreshold = 40;
	//[SerializeField] [Range(0.0f, 10.0f)] private float _directionChangeTime = 1.5f;
	[SerializeField] [ReadOnly] private bool _feeding;
	[SerializeField] private float _minForceToKnockBack = 50f;
	[SerializeField] protected SphereCollider _targetTrigger;

	[SerializeField] private ParticleSystem _impactEffect;

	protected Vector3 _preDestination = Vector3.positiveInfinity;
	private float _directionChangeTimer;
	private int _feedingHash = Animator.StringToHash("Feeding");

	public bool Feeding { set { _feeding = value; } }
	public float MaxSatisfaction { get { return 1.0f; } }
	public float Satisfaction { get { return _satisfaction; } set { _satisfaction = value; } }
	public float SlerpSpeed { set { _slerpSpeed = value; } }
	public SphereCollider TargetTrigger { get { return _targetTrigger; } set { _targetTrigger = value; } }

	protected override void Update()
	{
		_satisfaction -= Time.deltaTime * _satisfactionDecrease;
		_satisfaction = _satisfaction < 0 ? 0 : _satisfaction;
		//_directionChangeTimer += Time.deltaTime;
		//Seeking();
		base.Update();
	}

	public override HashSet<KeyValuePair<string, object>> CreateGoalState()
	{
		HashSet<KeyValuePair<string, object>> goal = new HashSet<KeyValuePair<string, object>>();
		goal.Add(new KeyValuePair<string, object>("patrol", true));
		goal.Add(new KeyValuePair<string, object>("idle", true));
		goal.Add(new KeyValuePair<string, object>("eat", true));
		//goal.Add(new KeyValuePair<string, object>("stayAlive", true));
		goal.Add(new KeyValuePair<string, object>("attack", true));
		goal.Add(new KeyValuePair<string, object>("pursuit", true));
		goal.Add(new KeyValuePair<string, object>("walk", true));

		return goal;
	}

	public override HashSet<KeyValuePair<string, object>> GetWorldState()
	{
		HashSet<KeyValuePair<string, object>> worldData = new HashSet<KeyValuePair<string, object>>();
		worldData.Add(new KeyValuePair<string, object>("isHungry", _satisfaction < _satisfactionThreshold));
		return worldData;
	}

	public override bool MoveAgent(GoapAction nextAction)
	{
		if (!_navAgent.enabled || nextAction.Target == null)
		{
			return true;
		}

		Vector3 targetPos = nextAction.Target.transform.position;
		targetPos.y = transform.position.y;
		if (_preDestination != targetPos || _navAgent.pathStatus == NavMeshPathStatus.PathInvalid || _navAgent.isPathStale /* && !agent.hasPath */)
		{
			_navAgent.SetDestination(targetPos);
			_preDestination = targetPos;
		}
		else if (_navAgent.remainingDistance <= _navAgent.stoppingDistance && !_navAgent.pathPending)
		{
			nextAction.InRange = true;
			_speed = 0;
			return true;
		}
		if (!UseRootRotation && _navAgent.desiredVelocity != Vector3.zero)
		{
			// Generate a new Quaternion representing the rotation we should have
			Quaternion newRot = Quaternion.LookRotation(_navAgent.desiredVelocity);
			// Smoothly rotate to that new rotation over time
			_goapAgent.transform.rotation = Quaternion.Slerp(_goapAgent.transform.rotation, newRot, Time.deltaTime * _slerpSpeed);
		}

		_speed = nextAction.Speed;
		_navAgent.speed = _speed;

		//if (_targetType == AITargetType.Waypoint && ! _navAgent.pathPending)
		//{
		//}
		return false;
	}

	public override void UpdateAnimatorParams()
	{
		base.UpdateAnimatorParams();
		_animator.SetBool(_feedingHash, _feeding);
	}

	public override void TakeDamage(Vector3 position, Vector3 force, float damage, Rigidbody bodyPart, int hitDirection = 0)
	{
		EmitBlood(position, Quaternion.Euler(-force.normalized));
		if (_health <= 0)
		{
			bodyPart.AddForce(force, ForceMode.Impulse);
			return;
		}
		_health -= damage;
		if(_health <= 0)
		{
			Die(force, bodyPart);
			return;
		}

		float hitStrenght = force.magnitude;
		if (hitStrenght > _minForceToKnockBack)
		{
			KnockBack(force, bodyPart);
		}
	}

	private void KnockBack(Vector3 force, Rigidbody bodyPart)
	{
		//TODO
	}

	protected override void Die(Vector3 force, Rigidbody bodyPart)
	{
		_navAgent.enabled = false;
		_animator.enabled = false;
		if(_collider)
			_collider.enabled = false;

		foreach (Rigidbody body in _bodyParts)
		{
			body.isKinematic = false;
		}
		_rigidbody.isKinematic = true;
		bodyPart.AddForce(force, ForceMode.Impulse);
		MonoBehaviour[] scripts = gameObject.GetComponents<MonoBehaviour>();
		MonoBehaviour skiped = null;
		foreach (MonoBehaviour script in scripts)
		{
			if(script == this)
			{
				skiped = script;
				continue;
			}
			script.enabled = false;
		}
		if(_sensorTrigger)
			_sensorTrigger.enabled = false;

		if (_targetTrigger)
			_targetTrigger.enabled = false;

		GameResourceManager.Instance.AddDeadZombie(gameObject);

		if (skiped)
			skiped.enabled = false;
		
	}
	//private void Seeking()
	//{
	//	float angle = MathFunctions.FindSignedAngle(transform.forward, _navAgent.steeringTarget - transform.position);
	//	if (Mathf.Abs(angle) > _waypointAngleThreshold)
	//	{
	//		if (_directionChangeTimer > _directionChangeTime)
	//		{
	//			_seeking = (int)Mathf.Sign(angle);
	//			_directionChangeTimer = 0.0f;
	//			Debug.Log("SEEKING: " + angle);
	//		}
	//	}
	//	else
	//	{
	//		_seeking = 0;
	//	}
	//}

	private void EmitBlood(Vector3 position, Quaternion rotation)
	{
		ParticleSystem bloodParticles = Instantiate(_impactEffect);
		bloodParticles.transform.position = position;
		bloodParticles.transform.rotation = rotation;
		ParticleSystem.MainModule main = bloodParticles.main;
		main.simulationSpace = ParticleSystemSimulationSpace.World;
		bloodParticles.Emit(60);
		Destroy(bloodParticles.gameObject, 2);
	}
}
