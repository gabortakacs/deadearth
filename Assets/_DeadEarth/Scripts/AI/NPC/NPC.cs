﻿using UnityEngine;

public abstract class NPC : MonoBehaviour
{
	[Header("NPC")]
	[SerializeField] protected Rigidbody _rigidbody;
	[SerializeField] [Range(0, 100)] protected float _health = 100;
	[SerializeField] protected float _meleeDamage = 10.0f;

	public float MeleeDamage { get { return _meleeDamage; } }

	protected abstract void Die(Vector3 force, Rigidbody rigidbody);
	public abstract void TakeDamage(Vector3 position, Vector3 force, float damage, Rigidbody rigidbody, int hitDirection = 0);

}
