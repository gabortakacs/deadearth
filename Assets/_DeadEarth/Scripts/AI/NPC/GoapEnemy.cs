﻿using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class GoapEnemy : GoapNPC
{
	[Header("Enemy")]
	[SerializeField] [Range(0.0f, 1.0f)] protected float _aggression = 1.0f;
	[SerializeField] [Range(0.0f, 1.0f)] protected float _intelligence = 0.5f;
	[SerializeField] [Range(0.0f, 1.0f)] protected float _sightLight = 0.75f;
	[SerializeField] private GameObject _pursuitTargetTrigger;

	protected int _seeking = 0;
	//private int _feedingHash = Animator.StringToHash("Feeding");
	private int _seekingHash = Animator.StringToHash("Seeking");

	public int Seeking { set { _seeking = value; } }
	public GameObject PursuitTargetTrigger { get { return _pursuitTargetTrigger; } }

	protected override void Start()
	{
		base.Start();
		GameResourceManager.Instance.RegisterGoapNPC(_sensorTrigger.GetInstanceID(), this);
	}

	protected override void Update()
	{
		base.Update();
		if (_navAgent.hasPath)
		{
			Vector3 toTarget = _navAgent.steeringTarget - this.transform.position;
			float turnAngle = Vector3.Angle(this.transform.forward, toTarget);
			_navAgent.acceleration = turnAngle * _navAgent.speed;
		}
	}

	public override void ActionsFinished()
	{
		//Debug.Log("<color=green>ActionsFinished</color>");
	}

	public override void PlanAborted(GoapAction aborter)
	{
		//Debug.Log("<color=yellow>PlanAborted</color>");
	}

	public override void PlanFailed(HashSet<KeyValuePair<string, object>> failedGoal)
	{
		//Debug.Log("<color=red>PlanFailed</color>");
	}

	public override void PlanFound(HashSet<KeyValuePair<string, object>> goal, Queue<GoapAction> actions)
	{
		//Debug.Log("<color=green>PlanFound</color>");
	}

	public override void UpdateAnimatorParams()
	{
		base.UpdateAnimatorParams();
		_animator.SetInteger(_seekingHash, _seeking);
		////Debug.Log(_seeking);
		if (_seeking != 0)
		{
			_animator.SetFloat(_speedHash, 0);
		}
		//_animator.SetBool(_feedingHash, _feeding);
	}

	public override void OnTriggerEvent(AITriggerEventType eventType, Collider other)
	{
		if (eventType == AITriggerEventType.Exit)
			return;

		AITargetType curType = _visualThreat.Type;
		if (other.CompareTag("Player"))
		{
			TargetingPlayer(other, curType);
		}
		else if (other.CompareTag("Flash Light") && curType != AITargetType.Visual_Player)
		{
			TargetingFlashLight(curType, other);
		}
		else if (other.CompareTag("AI Sound Emitter"))
		{
			TargetingAudioEmitter(other);
		}
		else if (other.CompareTag("AI Food") && curType != AITargetType.Visual_Player && curType != AITargetType.Visual_Light && _audioThreat.Type == AITargetType.None)
		{
			TargetingFood(other);
		}
	}

	protected virtual void TargetingPlayer(Collider other, AITargetType curType)
	{
		float distance = Vector3.Distance(SensorPosition, other.transform.position);
		if (curType == AITargetType.Visual_Player && distance >= _visualThreat.Distance)
			return;

		TargetingVisualThreat(other, AITargetType.Visual_Player, _sight, distance);
	}

	//protected virtual void TargetingFlashLight(AITargetType curType, Collider other)
	//{
	//	BoxCollider flashLightTrigger = (BoxCollider)other;
	//	float distanceToThreat = Vector3.Distance(SensorPosition, flashLightTrigger.transform.position);
	//	float zSize = flashLightTrigger.size.z * flashLightTrigger.transform.lossyScale.z;
	//	float aggrFactor = distanceToThreat / zSize;
	//	if (aggrFactor <= _sight && aggrFactor <= _intelligence)
	//	{
	//		_visualThreat.Set(AITargetType.Visual_Light, other, other.transform.position, distanceToThreat);
	//	}
	//}

	protected virtual void TargetingFlashLight(AITargetType curType, Collider other)
	{
		TargetingVisualThreat(other, AITargetType.Visual_Light, _sightLight);
	}

	protected virtual void TargetingFood(Collider other)
	{
		TargetingVisualThreat(other, AITargetType.Visual_Food, _sight);
	}

	private void TargetingVisualThreat(Collider other, AITargetType otherType, float sightModifier)
	{
		float distanceToThreat = Vector3.Distance(other.transform.position, SensorPosition);
		if (distanceToThreat >= _visualThreat.Distance)
			return;
		TargetingVisualThreat(other, otherType, sightModifier, distanceToThreat);
	}

	private void TargetingVisualThreat(Collider other, AITargetType otherType, float sightModifier, float distanceToThreat)
	{
		int layerMask = otherType == AITargetType.Visual_Player ? LayerMasks.PlayerLayerMask : LayerMasks.VisualLayerMask;
		RaycastHit hitInfo;
		if (ColliderIsVisible(other, out hitInfo, layerMask, sightModifier))
		{
			_visualThreat.Set(otherType, other, other.transform.position, distanceToThreat);
		}
	}

	protected virtual void TargetingAudioEmitter(Collider other)
	{
		if (other.GetType() != typeof(SphereCollider))
			return;
		SphereCollider soundTrigger = (SphereCollider)other;

		Vector3 agentSensorPosition = SensorPosition;

		Vector3 soundPosition;
		float soundRadius;
		MathFunctions.ConvertSphereColliderToWorldSpace(soundTrigger, out soundPosition, out soundRadius);

		float distanceToThreat = (soundPosition - agentSensorPosition).magnitude;

		float distanceFactor = distanceToThreat / soundRadius;
		distanceFactor += distanceFactor * (1.0f - _hearing);

		if (distanceFactor > 1.0f)
			return;

		if (distanceToThreat < _audioThreat.Distance)
		{
			_audioThreat.Set(AITargetType.Audio, other, other.transform.position, distanceToThreat);
		}
	}
}
