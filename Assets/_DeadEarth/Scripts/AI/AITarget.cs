﻿using UnityEngine;

public struct AITarget
{
	private AITargetType _type;
	private Collider _collider;
	private Vector3 _position;
	private float _distance;
	private float _time;

	public AITargetType Type { get { return _type; } }
	public Collider Collider { get { return _collider; } }
	public Vector3 Position { get { return _position; } }
	public float Distance { get { return _distance; } set { _distance = value; } }
	public float Time { get { return _time; } set { _time = value; } }

	public void Set(AITargetType t, Collider c, Vector3 p, float d)
	{
		_type = t;
		_collider = c;
		_position = p;
		_distance = d;
		_time = UnityEngine.Time.time;
	}

	public void Clear()
	{
		_type = AITargetType.None;
		_collider = null;
		_position = Vector3.zero;
		_distance = Mathf.Infinity;
		_time = 0.0f;
	}

	public static AITarget GetHigherPrio(AITarget a, AITarget b)
	{
		return (int)a.Type <= (int)b.Type ? a : b;
	}

	public static int Compare(AITarget a, AITarget b)
	{
		return a.Type > b.Type ? -1 : a.Type == b.Type ? 0 : 1;
	}
}