﻿using UnityEngine;

public class EnemyMeleeAttack : MonoBehaviour 
{
	[SerializeField] private NPC _npc;
	[SerializeField] private float _attackPerSecond = 1.0f;

	private float _deactivateTime = Mathf.NegativeInfinity;

	private void OnTriggerEnter(Collider other)
	{
		if(other.tag == "Player" && _deactivateTime < Time.fixedTime)
		{
			Player.Instance.TakeDamage(_npc.MeleeDamage);
			_deactivateTime = Time.fixedTime + _attackPerSecond;
		}
	}
}
