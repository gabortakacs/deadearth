﻿using UnityEngine;
using Random = UnityEngine.Random;

public class GoapZombieMeleeAttack : GoapAction
{
	[SerializeField] private int _attackTypeNumber = 6;
	//[SerializeField] private float _range;

	private int _attackType = 0;
	private int _attackLayerIndex = -1;
	private int _emptyStateHash = -1;
	private float _attackEndTime = -1.0f;

	public GoapZombieMeleeAttack()
	{
		_name = "GoapZombieMeleeAttack";
		AddEffect("attack", true);
		SetNavAgentControlParams(true, false);
	}

	protected override void Start()
	{
		base.Start();
		if (_attackLayerIndex == -1)
			_attackLayerIndex = _goapNPC.Animator.GetLayerIndex("Attack Layer");
		_emptyStateHash = Animator.StringToHash("Empty State");
	}
	public override bool CheckProceduralPrecondition()
	{
		if (!enabled || _goapNPC.Target.Type != AITargetType.Visual_Player)
			return false;
		_target = _goapNPC.Target.Collider.gameObject;
		_attackType = Random.Range(0, _attackTypeNumber) + 1;
		return true;
	}

	public override bool IsDone()
	{
		return _completed;
	}

	public override bool Perform()
	{
		if (_attackEndTime == -1 && _goapNPC.Animator.GetCurrentAnimatorStateInfo(_attackLayerIndex).shortNameHash != _emptyStateHash)
		{
			_attackEndTime = Time.fixedTime + _goapNPC.Animator.GetCurrentAnimatorStateInfo(_attackLayerIndex).length*0.9f;
		}
		if(_attackEndTime < Time.fixedTime && _attackEndTime != -1)
		{
			_completed = true;
			_attackType = 0;
		}
		_goapNPC.AttackType = _attackType;
		return true;
	}

	public override bool RequiresInRange()
	{
		return true;
	}

	public override void Reset()
	{
		_attackType = 0;
		_completed = false;
		_attackEndTime = -1;
	}

	public override void OnAnimatorUpdated()
	{
		base.OnAnimatorUpdated();
		if (_goapNPC.AttackType != 0 && !_goapNPC.UseRootRotation)
		{
			Vector3 tmp = _target.transform.position;
			tmp.y = transform.position.y;
			_goapNPC.transform.LookAt(tmp);
		}
		//Debug.Log((_goapNPC.UseRootRotation + " " + _goapNPC._rootRotationRefCount));
	}
}
