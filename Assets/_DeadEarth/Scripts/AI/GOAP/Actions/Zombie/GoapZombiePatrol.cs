﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(GoapZombie))]
public class GoapZombiePatrol : GoapAction
{
	[Header("Patrol")]
	[SerializeField] private AIWaypointNetwork _network;
	[SerializeField] [Range(0, 100)] private int _chanceToPatrol = 80;
	[SerializeField] private float _minTimeUntilLastPatrol = 5.0f;
	[SerializeField] float _slerpSpeed = 5.0f;

	private GoapZombie _goapZombie;
	private int _currentIndex;
	private float _lastPatrolTime;

	public GoapZombiePatrol()
	{
		_name = "GoapZombiePatrol";
		AddEffect("patrol", true);
		_target = null;
		SetNavAgentControlParams(true, false);
		_lastPatrolTime = Mathf.NegativeInfinity;
	}

	protected void Awake()
	{
		_goapZombie = (GoapZombie)_goapNPC;
	}

	public override void Reset()
	{
		_completed = false;
		_target = null;
	}

	public override bool IsDone()
	{
		return _completed;
	}

	public override bool RequiresInRange()
	{
		return true;
	}

	public override bool CheckProceduralPrecondition()
	{
		if (!enabled || _lastPatrolTime + _minTimeUntilLastPatrol > Time.fixedUnscaledTime)
			return false;

		if (Random.Range(0.0f, 100.0f) > _chanceToPatrol)
		{
			return false;
		}
		_currentIndex = ++_currentIndex == _network.Waypoints.Count ? 0 : _currentIndex;
		_target = _network.Waypoints[_currentIndex];
		return _target != null;
	}

	public override bool Perform()
	{
		_completed = true;
		_lastPatrolTime = Time.fixedUnscaledTime;
		return true;
	}

	public override void SetGoapNPCParameters()
	{
		base.SetGoapNPCParameters();
		_goapZombie.SlerpSpeed = _slerpSpeed;
	}
}