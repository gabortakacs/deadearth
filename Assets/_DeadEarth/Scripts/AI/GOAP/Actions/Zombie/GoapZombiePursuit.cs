﻿using UnityEngine;

public class GoapZombiePursuit : GoapAction
{

	public GoapZombiePursuit()
	{
		_name = "GoapZombiePursuit";
		AddEffect("pursuit", true);
		SetNavAgentControlParams(true, false);
	}

	public override bool CheckProceduralPrecondition()
	{
		if (!enabled || _goapNPC.Target.Type != AITargetType.Audio && _goapNPC.Target.Type != AITargetType.Visual_Light)
			return false;

		_target = ((GoapZombie)_goapNPC).PursuitTargetTrigger;
		_target.transform.position = _goapNPC.Target.Collider.transform.position;
		return true;
	}

	public override bool IsDone()
	{
		if(_completed)
		{
			_goapNPC.ClearTarget();
		}
		return _completed;
	}

	public override bool Perform()
	{
		_completed = true;
		return true;
	}

	public override bool RequiresInRange()
	{
		return true;
	}

	public override void Reset()
	{
		_completed = false;
	}
}
