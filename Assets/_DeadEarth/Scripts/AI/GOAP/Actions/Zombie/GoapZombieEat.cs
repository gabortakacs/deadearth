﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(GoapZombie))]
public class GoapZombieEat : GoapAction
{
	[Header("Eating")]
	[SerializeField] [Range(0.0f, 0.1f)] private float _eatingSpeed = 0.01f;
	[SerializeField] ParticleSystem _bloodParticlesPrefab;
	[SerializeField] Transform _bloodParticlesMount;
	[SerializeField] [Range(0.01f, 1.0f)] private float _bloodParticlesBurstTime = 0.1f;
	[SerializeField] [Range(1, 100)] private int _bloodParticlesBurstAmount = 10;

	private GoapZombie _goapZombie;
	private int _eatingLayerIndex = -1;
	private int _eatingStateHash = Animator.StringToHash("Feeding State");
	private float _timer = Mathf.Infinity;
	private ParticleSystem _bloodParticles;
	private float _satisfactionMaxThreshold;

	public GoapZombieEat()
	{
		_name = "GoapZombieEat";
		AddEffect("eat", true);
		AddPrecondition("isHungry", true);
		_target = null;
		SetNavAgentControlParams(true, false);
	}

	protected override void Start()
	{
		base.Start();
		_goapZombie = (GoapZombie)_goapNPC;
		if (_eatingLayerIndex == -1)
			_eatingLayerIndex = _goapNPC.Animator.GetLayerIndex("Eating Layer");
		_satisfactionMaxThreshold = _goapZombie.MaxSatisfaction * 0.95f;
	}

	public override bool CheckProceduralPrecondition()
	{
		if (!enabled || _goapZombie.Target.Type != AITargetType.Visual_Food)
			return false;

		_target = _goapNPC.Target.Collider.gameObject;
		return true;
	}

	public override bool IsDone()
	{
		if (_goapZombie.Satisfaction >= _satisfactionMaxThreshold)
		{
			_goapZombie.Satisfaction = _goapZombie.MaxSatisfaction;
			_goapZombie.Feeding = false;
			return true;
		}
		return false;
	}

	public override bool Perform()
	{
		_goapZombie.Feeding = true;
		if(_goapNPC.Animator.GetCurrentAnimatorStateInfo(_eatingLayerIndex).shortNameHash != _eatingStateHash)
			return true;

		_timer += Time.deltaTime;
		if (_timer > _bloodParticlesBurstTime)
		{
			if (_bloodParticles == null)
			{
				_bloodParticles = Instantiate(_bloodParticlesPrefab);
				_bloodParticles.transform.parent = this.transform;
			}
			_bloodParticles.transform.position = _bloodParticlesMount.transform.position;
			_bloodParticles.transform.rotation = _bloodParticlesMount.transform.rotation;
			ParticleSystem.MainModule main = _bloodParticles.main;
			main.simulationSpace = ParticleSystemSimulationSpace.World;
			_bloodParticles.Emit(_bloodParticlesBurstAmount);
			_timer = 0.0f;
		}
		_goapZombie.Satisfaction += Time.deltaTime * _eatingSpeed;
		return true;
	}

	public override bool RequiresInRange()
	{
		return true;
	}

	public override void Reset()
	{
		_target = null;
	}

	public override void OnEnterState() {
		Debug.Log("Teszt");
	}
}
