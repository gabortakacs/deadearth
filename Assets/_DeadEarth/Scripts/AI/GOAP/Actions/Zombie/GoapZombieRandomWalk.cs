﻿using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class GoapZombieRandomWalk : GoapAction
{

	[SerializeField] [Range(0, 100)] private int _chanceToRandomWalk = 50;
	[SerializeField] private float walkRadius = 20.0f;
	[SerializeField] private float _navMeshStoppingDistance = 2.0f;

	private GoapZombie _goapZombie;
	private float _originalNavMeshStoppingDistance = 1;

	public GoapZombieRandomWalk()
	{
		_name = "GoapZombieRandomWalk";
		AddEffect("walk", true);
		SetNavAgentControlParams(true, false);
	}

	private void Awake()
	{
		_goapZombie = (GoapZombie)_goapNPC;
	}
	public override bool CheckProceduralPrecondition()
	{
		if (Random.Range(0.0f, 100.0f) > _chanceToRandomWalk)
		{
			return false;
		}
		_target = GetRandomTargetOnNavMesh();
		_originalNavMeshStoppingDistance = _goapNPC.NavAgent.stoppingDistance;
		_goapNPC.NavAgent.stoppingDistance = _navMeshStoppingDistance;
		return true;
	}

	private GameObject GetRandomTargetOnNavMesh()
	{
		Vector3 randomDirection = Random.insideUnitSphere * walkRadius;
		randomDirection += transform.position;
		NavMeshHit hit;
		NavMesh.SamplePosition(randomDirection, out hit, walkRadius, 1);
		GameObject newTarget = _goapZombie.TargetTrigger.gameObject;
		newTarget.transform.position = hit.position;
		return newTarget;
	}

	public override bool IsDone()
	{
		if(_completed)
			_goapNPC.NavAgent.stoppingDistance = _originalNavMeshStoppingDistance;
		return _completed;
	}

	public override bool Perform()
	{
		_completed = true;
		return true;
	}

	public override bool RequiresInRange()
	{
		return true;
	}

	public override void Reset()
	{
		_completed = false;
		_target = null;
	}
}

