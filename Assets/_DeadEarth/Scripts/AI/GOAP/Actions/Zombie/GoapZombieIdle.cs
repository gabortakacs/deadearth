﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoapZombieIdle : GoapAction {

	[Header("Idle")]
	[SerializeField] private float _duration = 10.0f;
	[Header("Seeking")]
	[SerializeField] [Range(0, 100)] private int _chanceToSeeking = 25;
	[SerializeField] private float _seekingRerollPeriod = 3.0f;
	[SerializeField] private float _seekingDuration = 2.0f;

	public float _timer;
	private float _seekingTimer;
	private float _seekingRerollTimer;
	private GoapZombie _goapZombie;

	public GoapZombieIdle()
	{
		_name = "GoapZombieIdle";
		AddEffect("idle", true);
		_completed = false;
		_timer = 0.0f;
		_seekingTimer = Mathf.Infinity;
		_seekingRerollTimer = Mathf.Infinity;
		SetNavAgentControlParams(true, false);
	}

	protected override void Start()
	{
		base.Start();
		_goapZombie = (GoapZombie)_goapNPC;
	}

	public override bool CheckProceduralPrecondition()
	{
		return enabled;
	}

	public override bool IsDone()
	{
		_goapZombie.Seeking = 0;
		return _completed;
	}

	public override bool Perform()
	{
		_timer += Time.deltaTime;
		_seekingTimer += Time.deltaTime;
		_seekingRerollTimer += Time.deltaTime;

		if (_timer >= _duration)
		{
			_completed = true;
			return true;
		}

		if (_seekingTimer < _seekingDuration)
			return true;

		if (_seekingRerollTimer > _seekingRerollPeriod)
		{
			((GoapZombie)_goapNPC).Seeking = 0;
			return true;
		}
		_seekingRerollTimer = 0.0f;
		if (Random.Range(0, 100) < _chanceToSeeking)
		{
			_goapZombie.Seeking = (int)Mathf.Sign(Random.Range(-1, 1));
			_seekingTimer = 0.0f;
		}
		return true;
	}

	public override bool RequiresInRange()
	{
		return false;
	}

	public override void Reset()
	{
		_completed = false;
		_timer = 0.0f;
		_seekingTimer = _seekingDuration;
		_seekingRerollTimer = _seekingRerollPeriod;
	}
}
