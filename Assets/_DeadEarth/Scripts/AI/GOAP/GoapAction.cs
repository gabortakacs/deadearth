using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(GoapNPC))]
public abstract class GoapAction : MonoBehaviour
{
	[Header("Main")]
	[SerializeField] protected float _cost;
	[SerializeField] protected GameObject _target;
	[SerializeField] [Range(0, 3)] protected int _speed;
	[SerializeField] protected GoapNPC _goapNPC;

	private HashSet<KeyValuePair<string, object>> _preconditions;
	private HashSet<KeyValuePair<string, object>> _effects;
	private bool _inRange = false;
	private bool _updateNavAgentPosition;
	private bool _updateNavAgentRotation;

	protected string _name;
	protected bool _completed = false;

	public int Speed { get { return _speed; } }
	public bool InRange { get { return _inRange; } set { _inRange = value; } }
	public float Cost { get { return _cost; } }
	public string ActionName { get { return _name; } }
	public GameObject Target { get { return _target; } set { _target = value; } }
	public HashSet<KeyValuePair<string, object>> Preconditions { get { return _preconditions; } }
	public HashSet<KeyValuePair<string, object>> Effects { get { return _effects; } }
	public bool UpdateNavAgentPosition { get { return _updateNavAgentPosition; } }
	public bool UpdateNavAgentRotation { get { return _updateNavAgentRotation; } }

	public abstract void Reset();
	public abstract bool IsDone();
	public abstract bool CheckProceduralPrecondition();
	public abstract bool Perform();
	public abstract bool RequiresInRange();

	public GoapAction()
	{
		_preconditions = new HashSet<KeyValuePair<string, object>>();
		_effects = new HashSet<KeyValuePair<string, object>>();
	}

	protected virtual void Start()
	{
		if(_goapNPC == null)
			_goapNPC = GetComponent<GoapNPC>();
	}

	public virtual void SetGoapNPCParameters()
	{
		_goapNPC.NavAgentControl(this);
	}

	public void DoReset()
	{
		_inRange = false;
		_target = null;
		Reset();
	}

	public void AddPrecondition(string key, object value)
	{
		_preconditions.Add(new KeyValuePair<string, object>(key, value));
	}

	public void RemovePrecondition(string key)
	{
		KeyValuePair<string, object> remove = default(KeyValuePair<string, object>);
		foreach (KeyValuePair<string, object> kvp in _preconditions)
		{
			if (kvp.Key.Equals(key))
			{
				remove = kvp;
			}
			if (!default(KeyValuePair<string, object>).Equals(remove))
			{
				_preconditions.Remove(remove);
			}
		}
	}

	public void AddEffect(string key, object value)
	{
		_effects.Add(new KeyValuePair<string, object>(key, value));
	}

	public void RemoveEffect(string key)
	{
		KeyValuePair<string, object> remove = default(KeyValuePair<string, object>);
		foreach (KeyValuePair<string, object> kvp in _effects)
		{
			if (kvp.Key.Equals(key))
			{
				remove = kvp;
			}
			if (!default(KeyValuePair<string, object>).Equals(remove))
			{
				_effects.Remove(remove);
			}
		}
	}

	protected void SetNavAgentControlParams(bool positionUpdate, bool rotationUpdate)
	{
		_updateNavAgentPosition = positionUpdate;
		_updateNavAgentRotation = rotationUpdate;
	}

	public virtual void OnAnimatorUpdated()
	{
		if (_goapNPC.UseRootPosition)
			_goapNPC.NavAgent.velocity = _goapNPC.Animator.deltaPosition / Time.deltaTime;

		if (_goapNPC.UseRootRotation)
			_goapNPC.transform.rotation = _goapNPC.Animator.rootRotation;
	}

	public virtual void OnEnterState() { }
	public virtual void OnExitState() { }
	public virtual void OnAnimatorIKUpdated() { }
	public virtual void OnTriggerEvent(AITriggerEventType eventType, Collider other) { }

	protected virtual bool ColliderIsVisible(Collider other, out RaycastHit hitInfo, int playerLayerMask)
	{
		hitInfo = new RaycastHit();

		if (typeof(GoapEnemy) != _goapNPC.GetType())
			return false;

		GoapEnemy goapEnemy = (GoapEnemy)_goapNPC;

		Vector3 head = goapEnemy.SensorPosition;
		Vector3 direction = other.transform.position - head;
		float angle = Vector3.Angle(direction, transform.forward);

		if (angle > goapEnemy.Fov * 0.5f)
			return false;

		RaycastHit[] hits = Physics.RaycastAll(head, direction.normalized, goapEnemy.SensorRadius * goapEnemy.Sight, playerLayerMask);

		float closestColliderDistance = float.MaxValue;
		Collider closestCollider = null;
		foreach (RaycastHit hit in hits)
		{
			if (hit.distance < closestColliderDistance)
			{
				if (hit.transform.gameObject.layer == LayerMasks.BodyPartSingleLayerMask)
				{
					if (goapEnemy != GameResourceManager.Instance.GetGoapNPC(hit.rigidbody.GetInstanceID()))
					{
						closestColliderDistance = hit.distance;
						closestCollider = hit.collider;
						hitInfo = hit;
					}
				}
				else
				{
					closestColliderDistance = hit.distance;
					closestCollider = hit.collider;
					hitInfo = hit;
				}
			}
		}

		return closestCollider && closestCollider.gameObject == other.gameObject;
	}

}
