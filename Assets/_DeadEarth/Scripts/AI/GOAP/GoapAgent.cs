﻿using UnityEngine;
using System.Collections.Generic;
using System;

[RequireComponent(typeof(GoapNPC))]
public sealed class GoapAgent : MonoBehaviour
{

	[SerializeField] [ReadOnly] private GoapAction _currentAction;

	private FSM _stateMachine;
	private FSM.FSMState _idleState;
	private FSM.FSMState _moveToState;
	private FSM.FSMState _performActionState;

	private HashSet<GoapAction> _availableActions;
	private Queue<GoapAction> _currentActions;
	private IGoap _dataProvider;
	private GoapPlanner _planner;
	private GoapNPC _goapNPC;

	private bool _planInterrupted = false;

	public GoapAction CurrentAction { get { return _currentAction; } }
	public bool PlanInterrupted
	{
		set
		{
			_planInterrupted = value;
		}
	}

	void Start()
	{
		_stateMachine = new FSM();
		_availableActions = new HashSet<GoapAction>();
		_currentActions = new Queue<GoapAction>();
		_planner = new GoapPlanner();
		_goapNPC = GetComponent<GoapNPC>();
		_goapNPC.OnAITargetChanged += OnAITargetChanged;
		FindDataProvider();
		CreateIdleState();
		CreateMoveToState();
		CreatePerformActionState();
		_stateMachine.PushState(_idleState);
		LoadActions();
	}

	void Update()
	{
		_stateMachine.Update(this.gameObject);
	}

	public void AddAction(GoapAction action)
	{
		_availableActions.Add(action);
	}

	public GoapAction GetAction(Type action)
	{
		foreach (GoapAction currAction in _availableActions)
		{
			if (currAction.GetType().Equals(action))
			{
				return currAction;
			}
		}

		return null;
	}

	public void RemoveAction(GoapAction action)
	{
		_availableActions.Remove(action);
	}

	private bool HasActionPlan()
	{
		return _currentActions.Count > 0;
	}

	private void CreateIdleState()
	{
		_idleState = (fsm, obj) => {

			if (_planInterrupted)
			{
				InterruptPlan(fsm);
				return;
			}
			HashSet<KeyValuePair<string, object>> worldState = _dataProvider.GetWorldState();
			HashSet<KeyValuePair<string, object>> goal = _dataProvider.CreateGoalState();

			Queue<GoapAction> plan = _planner.Plan(_availableActions, worldState, goal);
			if (plan != null)
			{
				_currentActions = plan;
				_dataProvider.PlanFound(goal, plan);

				fsm.PopState();
				fsm.PushState(_performActionState);
			}
			else
			{
				_dataProvider.PlanFailed(goal);
				fsm.PopState();
				fsm.PushState(_idleState);
			}
		};
	}

	private void CreateMoveToState()
	{
		_moveToState = (fsm, gameObject) => {

			if(_planInterrupted)
			{
				InterruptPlan(fsm);
				return;
			}
			_currentAction = _currentActions.Peek();
			_currentAction.SetGoapNPCParameters();
			if (_currentAction.RequiresInRange() && _currentAction.Target == null)
			{
				fsm.PopState();
				fsm.PopState();
				fsm.PushState(_idleState);
				return;
			}

			if (_dataProvider.MoveAgent(_currentAction))
			{
				fsm.PopState();
			}

		};
	}

	private void CreatePerformActionState()
	{

		_performActionState = (fsm, obj) => {

			if (_planInterrupted)
			{
				InterruptPlan(fsm);
				return;
			}
			if (!HasActionPlan())
			{
				fsm.PopState();
				fsm.PushState(_idleState);
				_dataProvider.ActionsFinished();
				return;
			}

			_currentAction = _currentActions.Peek();
			_currentAction.SetGoapNPCParameters();
			if (_currentAction.IsDone())
			{
				_currentActions.Dequeue();
			}

			if (HasActionPlan())
			{
				_currentAction = _currentActions.Peek();
				bool inRange = _currentAction.RequiresInRange() ? _currentAction.InRange : true;

				if (inRange)
				{
					bool success = _currentAction.Perform();
					if (!success)
					{
						fsm.PopState();
						fsm.PushState(_idleState);
						CreateIdleState();
						_dataProvider.PlanAborted(_currentAction);
					}
				}
				else
				{
					fsm.PushState(_moveToState);
				}
			}
			else
			{
				fsm.PopState();
				fsm.PushState(_idleState);
				_dataProvider.ActionsFinished();
			}
		};
	}

	private void FindDataProvider()
	{
		foreach (Component comp in gameObject.GetComponents(typeof(Component)))
		{
			if (typeof(IGoap).IsAssignableFrom(comp.GetType()))
			{
				_dataProvider = (IGoap)comp;
				return;
			}
		}
	}

	private void LoadActions()
	{
		GoapAction[] actions = gameObject.GetComponents<GoapAction>();
		foreach (GoapAction a in actions)
		{
			_availableActions.Add(a);
		}
	}

	void OnAITargetChanged()
	{
		Debug.Log("<color=blue>" + _goapNPC.Target.Type + "</color>");
		_planInterrupted = true;

	}

	void InterruptPlan(FSM fsm)
	{
		Debug.Log("<color=red> Interrupted  </color>");
		GoapAction currentAction = _currentActions.Count == 0 ? null : _currentActions.Peek();
		_currentActions.Clear();
		fsm.PushState(_idleState);
		_dataProvider.PlanAborted(currentAction);
		_planInterrupted = false;
	}

	private void OnDestroy()
	{
		if (_goapNPC)
			_goapNPC.OnAITargetChanged -= OnAITargetChanged;
	}
}