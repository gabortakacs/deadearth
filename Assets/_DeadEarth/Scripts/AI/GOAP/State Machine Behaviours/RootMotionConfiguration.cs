﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RootMotionConfiguration : AIStateMachineLink
{
	[SerializeField] private int _rootPosition = 0;
	[SerializeField] private int _rootRotation = 0;

	public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		if(_goapNPC)
		{
			_goapNPC.AddRootMotionRequest(_rootPosition, _rootRotation);
		}
	}
	public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		if (_goapNPC)
			_goapNPC.AddRootMotionRequest(-_rootPosition, -_rootRotation);
	}

}
