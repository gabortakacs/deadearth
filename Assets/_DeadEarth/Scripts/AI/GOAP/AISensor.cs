﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AISensor : MonoBehaviour
{
	private GoapNPC _goapNPC;
	public GoapNPC GoapNPC { set { _goapNPC = value; } }

	public void OnTriggerEnter(Collider col)
	{
		if(_goapNPC != null)
			_goapNPC.OnTriggerEvent(AITriggerEventType.Enter, col);
	}
	public void OnTriggerStay(Collider col)
	{
		if (_goapNPC != null)
			_goapNPC.OnTriggerEvent(AITriggerEventType.Stay, col);
	}

	public void OnTriggerExit(Collider col)
	{
		if (_goapNPC != null)
			_goapNPC.OnTriggerEvent(AITriggerEventType.Exit, col);
	}
}
