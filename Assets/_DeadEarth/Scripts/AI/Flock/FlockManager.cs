﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlockManager : MonoBehaviour {


    [HideInInspector] public GameObject[] allFish;

    [SerializeField] private GameObject _collectorObject;
	[SerializeField] private GameObject[] _fishPrefabs;
	[SerializeField] private int _numFish = 20;
	[SerializeField] private Vector3 _swimLimits = new Vector3(5, 1, 5);
	[SerializeField] private Transform _goalPos;
	[SerializeField] [Range(0, 100)] private int _newGoalPosChance = 20;

    [Header("Fish Settings")]
	[SerializeField] [Range(0.0f, 5.0f)] private float _minSpeed;
	[SerializeField] [Range(0.0f, 5.0f)] private float _maxSpeed;
	[SerializeField] [Range(1.0f, 10.0f)] private float _neightbourDistance;
	[SerializeField] [Range(0.0f, 5.0f)] private float _rotationSpeed;

	public float MinSpeed { get { return _minSpeed; } }
	public float MaxSpeed { get { return _maxSpeed; } }
	public float RotationSpeed { get { return _rotationSpeed; } }
	public Vector3 SwimLimits { get { return _swimLimits; } }
	public float NeightbourDistance { get { return _neightbourDistance; } }
	public Transform GoalPos { get { return _goalPos; } }

	void Awake () {
        allFish = new GameObject[_numFish];
        for(int i = 0; i < _numFish; ++i)
        {
            Vector3 pos = this.transform.position + new Vector3(Random.Range(-_swimLimits.x, _swimLimits.x),
                                                                Random.Range(-_swimLimits.y, _swimLimits.y),
                                                                Random.Range(-_swimLimits.z, _swimLimits.z));
			int nextPrefabIndex = Random.Range(0, _fishPrefabs.Length);

			allFish[i] = Instantiate(_fishPrefabs[nextPrefabIndex], pos, Quaternion.identity);
            allFish[i].transform.parent = _collectorObject.transform;
            allFish[i].GetComponent<Flock>().MyManager = this;
		}
		_goalPos.position = transform.position;
	}
	
	void Update () {
		if (Random.Range(0, 100) < _newGoalPosChance)
			_goalPos.position = transform.position + new Vector3(Random.Range(-_swimLimits.x, _swimLimits.x),
																Random.Range(-_swimLimits.y, _swimLimits.y),
																Random.Range(-_swimLimits.z, _swimLimits.z));
	}
}
