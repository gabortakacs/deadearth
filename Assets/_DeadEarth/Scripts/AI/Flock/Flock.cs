﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flock : MonoBehaviour {

    private FlockManager _myManager;
    private float _speed;
	private bool turning;

	public FlockManager MyManager { get { return _myManager; } set { _myManager = value; } }

	void Start () {
        _speed = Random.Range(_myManager.MinSpeed, _myManager.MaxSpeed);
	}
	
	void Update () {

		Bounds b = new Bounds(_myManager.transform.position, _myManager.SwimLimits * 2);
		RaycastHit hit = new RaycastHit();
		Vector3 direction = Vector3.zero;
		if (!b.Contains(transform.position))
		{
			turning = true;
			direction = _myManager.transform.position - transform.position;
			//Debug.DrawLine(transform.position, direction);
		}
		else if(Physics.Raycast(transform.position, transform.forward, out hit, 50, LayerMasks.DefaultLayerMask))
		{
			turning = true;
			direction = Vector3.Reflect(transform.forward, hit.normal);
		}
		else
		{
			turning = false;
		}

		if (turning)
		{
			transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), _myManager.RotationSpeed * Time.deltaTime);
		}
		else
		{
			if (Random.Range(0, 100) < 10)
				_speed = Random.Range(_myManager.MinSpeed, _myManager.MaxSpeed);
			if (Random.Range(0, 100) < 20)
				ApplyRules();

			//Debug.DrawLine(transform.position, transform.forward*50, Color.blue);
		}

        transform.Translate(0, 0, Time.deltaTime * _speed);
	}

    void ApplyRules()
    {
        GameObject[] gos;
        gos = _myManager.allFish;

        Vector3 vcentre = Vector3.zero;
        Vector3 vavoid = Vector3.zero;

        float gSpeed = 0.01f;
        float nDistance;
        int groupSize = 0;

        foreach(GameObject go in gos)
        {
            nDistance = Vector3.Distance(go.transform.position, transform.position);
            if(nDistance <= _myManager.NeightbourDistance)
            {
                vcentre += go.transform.position;
                ++groupSize;

                if(nDistance < 1.0f)
                {
                    vavoid += (transform.position - go.transform.position);
                }

                Flock anotherFlock = go.GetComponent<Flock>();
                gSpeed += anotherFlock._speed;
            }
        }

        if(groupSize > 0)
        {
            vcentre = vcentre/groupSize + (_myManager.GoalPos.position - transform.position);
            _speed = gSpeed / groupSize;

            Vector3 direction = (vcentre + vavoid) - transform.position;
            if(direction != Vector3.zero)
            {
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), _myManager.RotationSpeed * Time.deltaTime);
            }
        }
    }
}
