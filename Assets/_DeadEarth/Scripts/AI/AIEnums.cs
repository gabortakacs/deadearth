﻿
public enum AITargetType
{
	None = 100,
	Visual_Player = 10,
	Visual_Light = 30,
	Audio = 40,
	Visual_Food = 50,
	Waypoint = 90
}

public enum AITriggerEventType { Enter, Stay, Exit }
