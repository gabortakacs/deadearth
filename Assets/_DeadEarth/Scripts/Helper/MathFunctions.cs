﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MathFunctions {
	
	public static void ConvertSphereColliderToWorldSpace(SphereCollider col, out Vector3 pos, out float radius)
	{
		pos = Vector3.zero;
		radius = 0.0f;
		if (col == null)
			return;

		pos = col.transform.position;
		pos.x += col.center.x * col.transform.lossyScale.x;
		pos.y += col.center.y * col.transform.lossyScale.y;
		pos.z += col.center.z * col.transform.lossyScale.z;

		float maxScale = Mathf.Max(col.transform.lossyScale.x, col.transform.lossyScale.y);
		radius = col.radius * Mathf.Max(maxScale, col.transform.lossyScale.z);
	}

	public static float FindSignedAngle(Vector3 fromVector, Vector3 toVector)
	{
		if (fromVector == toVector)
			return 0.0f;

		float angle = Vector3.Angle(fromVector, toVector);
		Vector3 cross = Vector3.Cross(fromVector, toVector);
		angle *= Mathf.Sign(cross.y);
		return angle;
	}
}
