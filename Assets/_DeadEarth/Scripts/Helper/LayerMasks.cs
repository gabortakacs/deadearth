﻿using UnityEngine;

public static class LayerMasks 
{
	public static int PlayerLayerMask = LayerMask.GetMask("Player", "AI Body Part") + 1;
	public static int VisualLayerMask = LayerMask.GetMask("Player", "AI Body Part", "Visual Aggravator") + 1;
	public static int ShotableLayerMask = LayerMask.GetMask("AI Body Part", "Water", "Terrain") + 1;
	public static int DefaultLayerMask = LayerMask.GetMask("Default") + 1;

	public static int BodyPartSingleLayerMask = LayerMask.NameToLayer("AI Body Part");
	public static int PlayerSingleLayerMask = LayerMask.NameToLayer("Player");
	public static int WaterSingleLayerMask = LayerMask.NameToLayer("Water");
	public static int MinimapSingleLayerMask = LayerMask.NameToLayer("Minimap");
	public static int TerrainSingleLayerMask = LayerMask.NameToLayer("Terrain");

	public static string HeadTag = "Head";
	public static string UpperBodyTag = "Upper Body";
	public static string LowerBodyTag = "Lower Body";

}
