﻿using System.Collections;
using UnityEngine;

public delegate void WeaponChanged();

public class WeaponController : MonoBehaviour
{

	[SerializeField] private int _currentWeapon;
	[SerializeField] private Weapon[] _weapons;
	[SerializeField] private float _weaponChangeBlockDuration = 0.1f;

	float _weaponChangeBlockTime = 0;

	public static event WeaponChanged OnWeaponChanged;

	void Start()
	{
		_weapons[_currentWeapon].gameObject.SetActive(true);
	}

	void Update ()
	{
		if (_weaponChangeBlockTime < Time.fixedTime)
		{
			float up = Input.GetAxis("Mouse ScrollWheel");
			if (up != 0.0f)
			{
				_weaponChangeBlockTime = Time.fixedTime + _weaponChangeBlockDuration;
				ChangeWeapon(up > 0);
			}
#if UNITY_EDITOR || UNITY_STANDALONE_WIN
			else
			{
				ChangeWeaponWithButton();
			}
#endif
		}
	}

	private void ChangeWeapon(bool up)
	{
		int increase = up ? 1 : -1;
		int nextIndex = _currentWeapon + increase;
		while (nextIndex != _currentWeapon)
		{
			if (nextIndex == _weapons.Length)
				nextIndex = 0;
			else if (nextIndex < 0)
				nextIndex = _weapons.Length - 1;
			if(ChangeWeapon(nextIndex))
				break;
			nextIndex += increase;
		}
	}

	private bool ChangeWeapon(int newIndex)
	{
		if (!_weapons[newIndex].IsAvailable)
			return false;

		_weapons[_currentWeapon].gameObject.SetActive(false);
		_weapons[newIndex].gameObject.SetActive(true);
		_currentWeapon = newIndex;
		if(OnWeaponChanged != null)
			OnWeaponChanged.Invoke();
		return true;
	}

#if UNITY_EDITOR ||  UNITY_STANDALONE_WIN
	private void ChangeWeaponWithButton()
	{
		for(int i = 0; i < _weapons.Length; ++i)
		{
			KeyCode keyCode = IntToKeyCode(i+1);
			if (keyCode != KeyCode.None && Input.GetKeyDown(keyCode) && i != _currentWeapon)
			{
				ChangeWeapon(i);
			}
		}
	}

	private KeyCode IntToKeyCode(int value)
	{
		switch(value)
		{
			case 0:
				return KeyCode.Alpha0;
			case 1:
				return KeyCode.Alpha1;
			case 2:
				return KeyCode.Alpha2;
			case 3:
				return KeyCode.Alpha3;
			case 4:
				return KeyCode.Alpha4;
			case 5:
				return KeyCode.Alpha5;
		}
		return KeyCode.None;
	}
#endif
}
