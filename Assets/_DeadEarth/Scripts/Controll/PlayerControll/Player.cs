﻿using UnityEngine;

public delegate void OnHealthChanged();
public delegate void OnDied();

public class Player : Singleton<Player> 
{
	[SerializeField] private float _health = 90.0f;
	[SerializeField] private float _maxHealth = 100.0f;
	[SerializeField] private int _regeneration = 10;
	[SerializeField] private float _regenerationDelayAfterHit = 10f;
	[SerializeField] private PlayerControllerBase _playerController;

	private float _regenerationTimer = Mathf.NegativeInfinity;

	public float Health { get { return _health; } }
	public float MaxHealth { get { return _maxHealth; } }
	public PlayerControllerBase PlayerContoller { get { return _playerController; } }

	public event OnHealthChanged OnPlayerHealthChanged;
	public event OnDied OnPlayerDied;

	private void Update()
	{
		if (_health < _maxHealth && _regenerationTimer < Time.fixedTime)
		{
			ChangeHP(_regeneration * Time.deltaTime);
		}
	}

	public void TakeDamage(float damage)
	{
		ChangeHP(-damage);
	}

	private void Die()
	{
		if(OnPlayerDied != null)
			OnPlayerDied.Invoke();
	}

	private void ChangeHP(float amount)
	{
		_health += amount;
		if (_health < 0)
		{
			Die();
			return;
		}

		_health = _health > _maxHealth ? _maxHealth : _health;

		if (amount < 0)
			_regenerationTimer = Time.fixedTime + _regenerationDelayAfterHit;

		if(OnPlayerHealthChanged != null)
			OnPlayerHealthChanged.Invoke();
	}
}
