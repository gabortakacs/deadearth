﻿using System.Collections;
using UnityEngine;

public delegate void StartReloading(float duration);
public delegate void InterruptReloading();
public delegate void BulletNumberInMagazineChanged(int current, int maxInMagazine, int all);

public class Weapon : MonoBehaviour, IPickableObject
{
	[SerializeField] private int _damage = 10;
	[SerializeField] private float _impactForce = 10f;
	[SerializeField] private float _range = 100f;
	[SerializeField] private float _fireRate = 15f;
	[SerializeField] private bool _auto;
	[SerializeField] private float _recoilAmountHorizontal = 0.15f;
	[SerializeField] private float _recoilAmountVertical = 0.15f;
	[SerializeField] private float _recoilDuration = 1.0f;
	[SerializeField] private float _firstShotLatencyAfterRun = 0.25f;
	[SerializeField] private Camera _mainFPSCamera;
	[SerializeField] private ParticleSystem _muzzleFlash;
	[SerializeField] private Scope _scope;
	[SerializeField] private AudioSource _shotSound;
	[SerializeField] private AudioSource _emptySound;
	[SerializeField] private Animator _animator;
	[SerializeField] private GameObject _impactEffectBase;
	[SerializeField] private GameObject _impactEffectWater;
	[SerializeField] private bool _muzzleFlashOnScoop = false;
	[SerializeField] private bool _isAvailable;
	[SerializeField] private int _allBulletNumber;
	[SerializeField] private int _maxBulletInMagazine = 30;
	[SerializeField] private float _reloadDuration= 3.0f;
	[SerializeField] private bool _discardAllUnusedBulletIfReload = true;


	private bool _isReloading = false;
	private float _shootLatencyTime;
	private float _nextTimeToFire = 0;
	private int _currentBulletInMagazine;
	private int _runHash;
	private PlayerControllerBase _playerContoller;
	private Coroutine _currentReloadCorountine;
	private Coroutine _currentShotSoundCorountine;

	public bool IsAvailable { get { return _isAvailable; } }
	public int AllBulletNumber { get { return _allBulletNumber; } }
	public int MaxBulletInMagazine { get { return _maxBulletInMagazine; } }
	public int CurrentBulletInMagazine { get { return _currentBulletInMagazine; } }

	public static event BulletNumberInMagazineChanged OnBulletNumberInMagazineChanged;
	public static event InterruptReloading OnInterruptReloading;
	public static event StartReloading OnStartReloading;

	private void Awake()
	{
		_currentBulletInMagazine = _maxBulletInMagazine;
		_runHash = Animator.StringToHash("Run");
		_playerContoller = Player.Instance.PlayerContoller;
	}

	private void OnEnable()
	{
		if(OnBulletNumberInMagazineChanged != null)
			OnBulletNumberInMagazineChanged.Invoke(_currentBulletInMagazine, _maxBulletInMagazine, _allBulletNumber);
	}

	private void OnDisable()
	{
		if (_isReloading)
			InterruptReloading();
	}

	private void Start()
	{
		print("Start");
		if (OnBulletNumberInMagazineChanged != null)
			OnBulletNumberInMagazineChanged.Invoke(_currentBulletInMagazine, _maxBulletInMagazine, _allBulletNumber);
	}

	private void Update()
	{
		if (!_playerContoller.IsWalking && (_playerContoller.MoveDirection.x != 0 || _playerContoller.MoveDirection.z != 0))
		{
			_shootLatencyTime = Time.fixedTime + _firstShotLatencyAfterRun;
			_animator.SetBool(_runHash, true);
			InterruptReloading();
			return;
		}
		_animator.SetBool(_runHash, false);

		if (_isReloading)
			return;

		if(Input.GetButtonDown("Reload"))
		{
			StartReloading();
			return;
		}

		if (_shootLatencyTime > Time.fixedTime)
			return;

		if ((_auto && Input.GetButton("Fire1") || Input.GetButtonDown("Fire1")) && Time.time >= _nextTimeToFire)
		{
			if (_currentBulletInMagazine == 0)
			{
				PlayEmptyShotSound();
				_nextTimeToFire = Time.time + 1f / _fireRate;
				return;
			}
			_nextTimeToFire = Time.time + 1f / _fireRate;
			Fire();
		}
	}

	public void PickedUp(int bulletAmount)
	{
		if (_isAvailable)
		{
			_allBulletNumber += bulletAmount;
		}
		else
		{
			_allBulletNumber = bulletAmount;
		}
		_isAvailable = true;

	}

	private void InterruptReloading()
	{
		if (!_isReloading)
			return;

		if (OnInterruptReloading != null)
			OnInterruptReloading.Invoke();
		if(_currentReloadCorountine != null)
			StopCoroutine(_currentReloadCorountine);
		_isReloading = false;
	}

	private void StartReloading()
	{
		if (_currentBulletInMagazine == _maxBulletInMagazine || _allBulletNumber == 0 || _isReloading)
			return;

		_isReloading = true;
		if (OnStartReloading != null)
			OnStartReloading.Invoke(_reloadDuration);
		_currentReloadCorountine = StartCoroutine(Reloading());
	}

	private IEnumerator Reloading()
	{
		yield return new WaitForSeconds(_reloadDuration);
		ReloadMagazine();
		if (OnBulletNumberInMagazineChanged != null)
			OnBulletNumberInMagazineChanged.Invoke(_currentBulletInMagazine, _maxBulletInMagazine, _allBulletNumber);
		_isReloading = false;
	}

	private void ReloadMagazine()
	{
		if(_discardAllUnusedBulletIfReload)
		{
			if (_allBulletNumber >= _maxBulletInMagazine)
			{
				_currentBulletInMagazine = _maxBulletInMagazine;
				_allBulletNumber -= _maxBulletInMagazine;
			}
			else
			{
				_currentBulletInMagazine = _allBulletNumber;
				_allBulletNumber = 0;
			}
		}
		else
		{
			if (_allBulletNumber >= _maxBulletInMagazine - _currentBulletInMagazine)
			{
				_allBulletNumber -= (_maxBulletInMagazine - _currentBulletInMagazine);
				_currentBulletInMagazine = _maxBulletInMagazine;
			}
			else
			{
				_currentBulletInMagazine = _allBulletNumber;
				_allBulletNumber = 0;
			}
		}
	}

	private void Fire()
	{
		PlayMuzzleFlash();
		Shoot();
		StartCoroutine(Recoil());
		PlayShotSound();
	}

	private IEnumerator Recoil()
	{
		float xRot = Random.Range(_recoilAmountVertical / 2, _recoilAmountVertical) / _recoilDuration;
		float yRot = Random.Range(-_recoilAmountHorizontal, _recoilAmountHorizontal) / _recoilDuration;
		float timer = 0;
		while(timer < _recoilDuration)
		{
			_playerContoller.MouseLook.LookRotation(_playerContoller.gameObject.transform, _mainFPSCamera.gameObject.transform, xRot * Time.deltaTime, yRot * Time.deltaTime);
			timer += Time.deltaTime;
			yield return null;
		}
	}

	private void Shoot()
	{
		RaycastHit hit;
		if (Physics.Raycast(_mainFPSCamera.transform.position, _mainFPSCamera.transform.forward, out hit, _range, LayerMasks.ShotableLayerMask))
		{
			Debug.Log(hit.transform.name + " " + hit.transform.gameObject.tag + " " + LayerMask.LayerToName(hit.transform.gameObject.layer));
			Target target = hit.transform.GetComponentInParent<Target>();
			if (target)
			{
				target.TakeDamage(hit.point, -hit.normal * _impactForce, _damage, hit.rigidbody);
			}
			else
			{
				GameObject impactGO;
				if (hit.transform.gameObject.layer == LayerMasks.WaterSingleLayerMask && _impactEffectWater != null)
				{
					impactGO = Instantiate(_impactEffectWater, hit.point, Quaternion.LookRotation(hit.normal));
				}
				else
				{
					impactGO = Instantiate(_impactEffectBase, hit.point, Quaternion.LookRotation(hit.normal));
				}
				Destroy(impactGO, 2.0f);
			}
		}
		--_currentBulletInMagazine;
		if(OnBulletNumberInMagazineChanged != null)
			OnBulletNumberInMagazineChanged.Invoke(_currentBulletInMagazine, _maxBulletInMagazine, _allBulletNumber);
	}

	private void PlayMuzzleFlash()
	{
		if (!_muzzleFlashOnScoop && (!_scope || !_scope.MuzzleflashDisabled))
			_muzzleFlash.Play();
	}

	private void PlayShotSound()
	{
		_shotSound.gameObject.SetActive(true);
		SoundMaster.Instance.PlaySound(_shotSound);
		if(_currentShotSoundCorountine != null)
		{
			StopCoroutine(_currentShotSoundCorountine);
		}
		_currentShotSoundCorountine = StartCoroutine(SetShotSoundGameObjectDeactive(_shotSound.clip.length));
	}

	private IEnumerator SetShotSoundGameObjectDeactive(float delay)
	{
		yield return new WaitForSeconds(delay);
		_shotSound.gameObject.SetActive(false);
	}

	private void PlayEmptyShotSound()
	{
		SoundMaster.Instance.PlaySound(_emptySound);
	}
}
