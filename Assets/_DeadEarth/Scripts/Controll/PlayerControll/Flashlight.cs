﻿using UnityEngine;

public class Flashlight : MonoBehaviour 
{

	[SerializeField] private Light _flashlight;
	[SerializeField] private bool _enabled;

	private bool _switchable = true;

	public bool Enabled { get { return _enabled; } }
	public bool Switchable { get { return _switchable; } set { _switchable = value; } }

	private void Start()
	{
		_flashlight.gameObject.SetActive(_enabled);
	}

	private void Update()
	{
		if(Input.GetButtonDown("Flashlight") && _switchable)
		{
			TurnOn(!_enabled);
		}
	}

	public void TurnOn(bool active)
	{
		_enabled = active;
		_flashlight.gameObject.SetActive(_enabled);
	}

}
