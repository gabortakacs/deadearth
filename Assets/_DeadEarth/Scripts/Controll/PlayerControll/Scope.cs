﻿using System.Collections;
using UnityEngine;

public abstract class Scope : MonoBehaviour 
{

	[SerializeField] protected Animator _animator;
	[SerializeField] protected float _onScopeDelay = 0.15f;
	[SerializeField] protected GameObject _weaponCamera;
	[SerializeField] protected float _scopedFov;
	[SerializeField] protected Camera _mainCamera;
	[SerializeField] protected FirstPersonController _player;
	[SerializeField] protected float _onScopeSensitivity = 1.0f;
	[SerializeField] protected float _scoopLatencyAfterRun = 0.25f;


	private float _latencyTimer;

	protected int _scopedHash;
	protected bool _isScoped = false;
	protected bool _isScopedDone = false;
	protected float _originalFov;
	protected float _originalXSensitivity;
	protected float _originalYSensitivity;
	protected bool _muzzleflashDisabled = false;

	//public bool IsScoped { get { return _isScoped; } }

	public bool MuzzleflashDisabled { get { return _muzzleflashDisabled; } }

	protected virtual void Start()
	{
		_scopedHash = Animator.StringToHash("Scoped");
		_originalFov = _mainCamera.fieldOfView;
		//TODO ezt gondot okoz majd ha allithato lesz a sensitivity
		_originalXSensitivity = _player.MouseLook.XSensitivity;
		_originalYSensitivity = _player.MouseLook.YSensitivity;
	}

	void Update()
	{
		if(!_player.IsWalking)
		{
			if (_isScoped)
			{
				_isScoped = false;
				OnUnScoped();
			}
			_latencyTimer = Time.fixedTime + _scoopLatencyAfterRun;
		}
		if (_latencyTimer > Time.fixedTime)
			return;

		if(Input.GetButtonDown("Fire2"))
		{
			_isScoped = !_isScoped;
			if (_isScoped)
				StartCoroutine(OnScoped());
			else
				OnUnScoped();
		}
		if(_isScopedDone && _mainCamera.fieldOfView != _scopedFov)
			_mainCamera.fieldOfView = _scopedFov;

	}

	protected abstract IEnumerator OnScoped();
	protected abstract void OnUnScoped();
}
