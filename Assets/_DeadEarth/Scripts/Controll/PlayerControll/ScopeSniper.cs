﻿using System.Collections;
using UnityEngine;
using UnityEngine.PostProcessing;

public class ScopeSniper : Scope 
{
	[SerializeField] private GameObject _scopeOverlay;
	[SerializeField] private bool _nightvision = true;
	[SerializeField] private PostProcessingProfile _nightvisionPostProcessingProfile;
	[SerializeField] protected Flashlight _flashlight;

	private PostProcessingBehaviour _mainCameraPPB;
	private PostProcessingProfile _originalPPP;
	private bool _originalFlashlightStatus;
	private bool _originalFlashlightSwitchable;

	protected override void Start()
	{
		base.Start();
		_mainCameraPPB = _mainCamera.GetComponent<PostProcessingBehaviour>();
		_originalPPP = _mainCameraPPB.profile;
	}

	protected override IEnumerator OnScoped()
	{
		_animator.SetBool(_scopedHash, _isScoped);
		yield return new WaitForSeconds(_onScopeDelay);
		_muzzleflashDisabled = true;
		FlashlightTurnOff();
		SetCamerasScoped();
		SetMouseSensitivityScoped();
		_isScopedDone = true;
	}

	protected override void OnUnScoped()
	{
		_animator.SetBool(_scopedHash, _isScoped);
		_muzzleflashDisabled = false;
		SetCamerasBack();
		FlashlightTurnBack();
		SetMouseSensitivityBack();
		_isScopedDone = false;
	}
	
	private void SetMouseSensitivityScoped()
	{
		_player.MouseLook.XSensitivity = _onScopeSensitivity;
		_player.MouseLook.YSensitivity = _onScopeSensitivity;
	}

	private void SetMouseSensitivityBack()
	{
		_player.MouseLook.XSensitivity = _originalXSensitivity;
		_player.MouseLook.YSensitivity = _originalYSensitivity;
	}

	private void SetCamerasScoped()
	{
		_scopeOverlay.SetActive(true);
		if (_nightvision)
			_mainCameraPPB.profile = _nightvisionPostProcessingProfile;
		_weaponCamera.SetActive(false);
		_mainCamera.fieldOfView = _scopedFov;
	}

	private void SetCamerasBack()
	{
		_weaponCamera.SetActive(true);
		_mainCameraPPB.profile = _originalPPP;
		_mainCamera.fieldOfView = _originalFov;
		_scopeOverlay.SetActive(false);
	}

	private void FlashlightTurnOff()
	{
		_originalFlashlightStatus = _flashlight.Enabled;
		_flashlight.TurnOn(false);
		_originalFlashlightSwitchable = _flashlight.Switchable;
		_flashlight.Switchable = false;
	}

	private void FlashlightTurnBack()
	{
		_flashlight.TurnOn(_originalFlashlightStatus);
		_flashlight.Switchable = _originalFlashlightSwitchable;
	}
}

