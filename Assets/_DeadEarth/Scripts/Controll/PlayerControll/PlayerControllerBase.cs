﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlayerControllerBase : MonoBehaviour {

	[SerializeField] protected MouseLook m_MouseLook;
	[SerializeField] protected bool m_IsWalking;

	protected Vector3 m_MoveDir = Vector3.zero;

	public MouseLook MouseLook { get { return m_MouseLook; } }
	public bool IsWalking { get { return m_IsWalking; } }
	public Vector3 MoveDirection { get { return m_MoveDir; } }
}
