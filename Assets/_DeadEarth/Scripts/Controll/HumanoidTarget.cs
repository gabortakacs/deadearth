﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class HumanoidTarget : Target
{
	[SerializeField] protected NPC _npc;
	[SerializeField] private float _headDamageMultiplier = 2.0f;
	[SerializeField] private float _upperBodyDamageMultiplier = 1.0f;
	[SerializeField] private float _lowerBodyDamageMultiplier = 0.5f;

	public override void TakeDamage(Vector3 position, Vector3 force, float damage, Rigidbody rigidbody, int hitDirection = 0)
	{ 
		_npc.TakeDamage(position, force, RecalculateDamage(damage, rigidbody), rigidbody, hitDirection);
	}

	private float RecalculateDamage(float damage, Rigidbody rigidbody)
	{
		if (rigidbody.gameObject.CompareTag(LayerMasks.HeadTag))
		{
			damage *= _headDamageMultiplier;
		}
		else if (rigidbody.gameObject.CompareTag(LayerMasks.UpperBodyTag))
		{
			damage *= _upperBodyDamageMultiplier;
		}
		else if (rigidbody.gameObject.CompareTag(LayerMasks.LowerBodyTag))
		{
			damage *= _lowerBodyDamageMultiplier;
		}
		else
		{
			Debug.Log("<color=red>Unexpected rigidbody.tag</color>");
		}
		return damage;
	}
}
