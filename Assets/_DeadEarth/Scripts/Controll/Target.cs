﻿using UnityEngine;

public abstract class Target : MonoBehaviour
{ 
	//public abstract void TakeDamage(Vector3 position, Vector3 force, float damage);
	public abstract void TakeDamage(Vector3 position, Vector3 force, float damage, Rigidbody rigidbody, int hitDirection = 0);
}
