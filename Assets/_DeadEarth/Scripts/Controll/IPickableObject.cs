﻿

public interface IPickableObject
{
	void PickedUp(int amount);
}