﻿using UnityEngine;

public delegate void PlayerInRange(bool inRange);

[RequireComponent(typeof(Collider))]
public class PickUpObject : MonoBehaviour
{
	[SerializeField] private GameObject _pickUpObject;

	[Tooltip("ha fegyver, akkor toltenyszamot jelent")]
	[SerializeField] private int _amount = 30;

	[SerializeField] private float _rotatingSpeed = 0.0f;

	public static event PlayerInRange OnPlayerInRange;

	private void Update()
	{
		if(_rotatingSpeed != 0)
		{
			transform.Rotate(Vector3.up * _rotatingSpeed * Time.deltaTime);
		}
	}


	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.layer != LayerMasks.PlayerSingleLayerMask)
			return;

		if (OnPlayerInRange != null)
			OnPlayerInRange.Invoke(true);
	}

	private void OnTriggerStay(Collider other)
	{
		if (other.gameObject.layer != LayerMasks.PlayerSingleLayerMask)
			return;

		if (Input.GetButtonDown("Use"))
		{
			print("USE");
			IPickableObject pickUpObject = _pickUpObject.GetComponent<IPickableObject>();
			pickUpObject.PickedUp(_amount);
			OnPlayerInRange.Invoke(false);
			Destroy(gameObject);
		}
	}
	private void OnTriggerExit(Collider other)
	{
		if (other.gameObject.layer != LayerMasks.PlayerSingleLayerMask)
			return;

		if (OnPlayerInRange != null)
			OnPlayerInRange.Invoke(false);
	}
}
