﻿using UnityEngine;

public class DestroyableTarget : Target
{

	[SerializeField] protected int _health = 50;
	[SerializeField] private GameObject _destroyedReplacementPrefab;
	[SerializeField] private Rigidbody _rigidbody;
	[SerializeField] private GameObject _impactEffect;
	public override void TakeDamage(Vector3 position, Vector3 force, float damage, Rigidbody rigidbody, int hitDirection = 0)
	{
		if (rigidbody)
		{
			rigidbody.AddForce(force);
		}
		_health -= (int)damage;
		if (_health <= 0)
		{
			Die();
		}
		GameObject impactGO = Instantiate(_impactEffect, position, Quaternion.LookRotation(-force.normalized));
		Destroy(impactGO, 2.0f);
	}

	private void Die()
	{
		if (_destroyedReplacementPrefab)
		{
			Instantiate(_destroyedReplacementPrefab, transform.position, transform.rotation);
		}
		Destroy(gameObject);
	}
}
