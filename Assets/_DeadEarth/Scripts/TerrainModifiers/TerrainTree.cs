﻿using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
public class TerrainTree : MonoBehaviour 
{

	public Terrain _terrain;
	public GameObject[] _parents;
	public GameObject[] _treePrefabs;

	private TreeInstance[] _originalTrees;

	private void Start()
	{
		//CopyTerrainTrees();
		//DeleteTerrainTrees();
	}

	void DeleteTerrainTrees()
	{
		List<TreeInstance> list = new List<TreeInstance>();
		_terrain.terrainData.treeInstances = list.ToArray();
	}

	void CopyTerrainTrees()
	{
#if UNITY_EDITOR
		_originalTrees = _terrain.terrainData.treeInstances;
		for (int i = 0; i < _originalTrees.Length; ++i)
		{
			TreeInstance terrainTree = _originalTrees[i];
			Quaternion rotation = Quaternion.Euler(0.0f, -8.38272f * terrainTree.rotation, 0.0f);
			GameObject replacementTree = PrefabUtility.InstantiatePrefab(_treePrefabs[terrainTree.prototypeIndex]) as GameObject;
			replacementTree.transform.position = Vector3.Scale(terrainTree.position, _terrain.terrainData.size);
			replacementTree.transform.localScale = Vector3.up * terrainTree.heightScale + Vector3.forward * terrainTree.widthScale + Vector3.right * terrainTree.widthScale;
			replacementTree.transform.rotation = rotation;
			replacementTree.transform.parent = _parents[terrainTree.prototypeIndex].transform;
			replacementTree.name += " (" + i + ")";
		}
#endif
	}

}

