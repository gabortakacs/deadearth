﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class TerrainGenerator : MonoBehaviour 
{

	public Terrain terrain;
	public int _depth = 20;

	public int _width = 256;
	public int _height = 256;
	public float _scale = 20;

	public float _offsetX = 0;
	public float _offsetY = 0;

	public float _compressionRatio = 3;
	public float _comressionOffset = 3;

	public AnimationCurve _curve;

	private void Start()
	{
	}

	private void Update()
	{
	}

	private void GenerateTerrain()
	{

		terrain.terrainData.heightmapResolution = _width + 1;
		terrain.terrainData.size = new Vector3(_width, _depth, _height);

		terrain.terrainData.SetHeights(0, 0, GenerateHeights());
	}

	float[,] GenerateHeights()
	{
		float[,] heights = new float[_width, _height];
		for(int x = 0; x < _width; ++x)
		{
			for(int y = 0; y < _height; ++y)
			{
				heights[x, y] = CalculateHeights(x, y);
			}
		}
		return heights;
	}

	private float CalculateHeights(int x, int y)
	{
		float xCoord = (float)x / _width * _scale + _offsetX;
		float yCoord = (float)y / _height * _scale + _offsetY;
		float a = Mathf.PerlinNoise(xCoord, yCoord);
		a /= _compressionRatio;
		a += 1/_comressionOffset;
		float xw = (float)x / _width;
		float yh = (float)y / _height;
		float multiplierX = _curve.Evaluate(xw);
		float multiplierY = _curve.Evaluate(yh);
		float multiplier = Mathf.Max(multiplierX, multiplierY);
		return Mathf.Lerp(0,1,a*multiplier);
	}
}
