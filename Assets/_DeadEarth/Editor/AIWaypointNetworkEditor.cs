﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.AI;

[CustomEditor(typeof(AIWaypointNetwork))]
public class AIWaypointNetworkEditor : Editor
{
	
	public override void OnInspectorGUI()
	{
		AIWaypointNetwork network = (AIWaypointNetwork)target;

		network.DisplayMode = (PathDisplayMode)EditorGUILayout.EnumPopup("DisplayMode", network.DisplayMode);
		if(network.DisplayMode == PathDisplayMode.Paths)
		{
			network.UIStart = (int)EditorGUILayout.IntSlider("UIStart", network.UIStart, 0, network.Waypoints.Count - 1);
			network.UIEnd = (int)EditorGUILayout.IntSlider("UIEnd", network.UIEnd, 0, network.Waypoints.Count - 1);
		}


		DrawDefaultInspector();

		if (GUI.changed)
			EditorUtility.SetDirty(target);
	}
	

	private void OnSceneGUI()
	{
		if (target == null)
			return;
		AIWaypointNetwork network = (AIWaypointNetwork)target;
		if (network == null || network.Waypoints == null || network.Waypoints.Count < 2)
			return;

		DrawNames(network);

		if(network.DisplayMode == PathDisplayMode.Connections)
		{
			DrawConnection(network);
		}
		else if(network.DisplayMode == PathDisplayMode.Paths)
		{
			DrawPath(network);
		}
	}

	private void DrawNames(AIWaypointNetwork network)
	{
		for (int i = 0; i < network.Waypoints.Count; ++i)
		{
			if (network.Waypoints[i] != null)
				Handles.Label(network.Waypoints[i].transform.position, "Waypoint " + i.ToString());
		}

	}

	private void DrawConnection(AIWaypointNetwork network)
	{
		Vector3[] linePoints = new Vector3[network.Waypoints.Count + 1];

		for (int i = 0; i <= network.Waypoints.Count; ++i)
		{
			int index = i != network.Waypoints.Count ? i : 0;
			if (network.Waypoints[index] != null)
				linePoints[i] = network.Waypoints[index].transform.position;
			else
				linePoints[i] = new Vector3(Mathf.Infinity, Mathf.Infinity, Mathf.Infinity);
		}

		Handles.color = Color.cyan;
		Handles.DrawPolyLine(linePoints);
	}

	private void DrawPath(AIWaypointNetwork network)
	{
		NavMeshPath path = new NavMeshPath();
		if (network.Waypoints[network.UIStart] == null || network.Waypoints[network.UIEnd] == null)
			return;
		Vector3 from = network.Waypoints[network.UIStart].transform.position;
		Vector3 to = network.Waypoints[network.UIEnd].transform.position;
		NavMesh.CalculatePath(from, to, NavMesh.AllAreas, path);
		if(path.status != NavMeshPathStatus.PathInvalid)
		{
			Handles.color = Color.yellow;
			Handles.DrawPolyLine(path.corners);
		}
	}
}
